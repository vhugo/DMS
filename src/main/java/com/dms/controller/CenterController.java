package com.dms.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dms.pojo.User;
import com.dms.service.IUserService;
import com.dms.utils.AjaxResult;
import com.dms.utils.JsonUtil;

@Controller
@RequestMapping("/Center")
public class CenterController {
	
	@Resource
	private IUserService userService;
	
	
	/**
	 * 跳转到登录页面
	 * 
	 * @return
	 */
	@RequestMapping("/index.do")
	public String index() {

		return "/view/login";
	}

	@RequestMapping(value = "/login.do", method = RequestMethod.POST)
	public void login(HttpServletRequest request, HttpServletResponse response,ModelMap map) {
		
		 AjaxResult<String> result = new AjaxResult<String>();
		 result.setCode(200);
         result.setMsg("usererror");
         try {
			response.getWriter().println(JsonUtil.toJson(result));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@RequestMapping("/toIndex.do")
	public String toIndex(){
		return "/view/index";
	}

}
