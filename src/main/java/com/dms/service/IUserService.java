package com.dms.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.dms.pojo.User;
import com.dms.pojo.UserExample;

public interface IUserService {
	long countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(String id);
    //新增根据邮箱返回user
    User selectByEmail(String email);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
}
