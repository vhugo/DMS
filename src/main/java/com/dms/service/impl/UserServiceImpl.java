package com.dms.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.dms.mapper.UserMapper;
import com.dms.pojo.User;
import com.dms.pojo.UserExample;
import com.dms.service.IUserService;
@Service
public class UserServiceImpl implements IUserService {
	
	
	@Resource
	private UserMapper userMapper;

	public long countByExample(UserExample example) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int deleteByExample(UserExample example) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int deleteByPrimaryKey(String id) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int insert(User record) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int insertSelective(User record) {
		// TODO Auto-generated method stub
		return 0;
	}

	public List<User> selectByExample(UserExample example) {
		// TODO Auto-generated method stub
		return null;
	}

	public User selectByPrimaryKey(String id) {
		return userMapper.selectByPrimaryKey(id);
	}
	public User selectByEmail(String email) {
		return userMapper.selectByEmail(email);
	}

	public int updateByExampleSelective(User record, UserExample example) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int updateByExample(User record, UserExample example) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int updateByPrimaryKeySelective(User record) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int updateByPrimaryKey(User record) {
		// TODO Auto-generated method stub
		return 0;
	}
}
